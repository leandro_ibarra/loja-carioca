<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProductController@home');
Route::get('/catalog', 'ProductController@index');
Route::get('/cart', 'CartController@index');
Route::get('/checkout', 'OrderController@index');

Route::get('/product/add/{id}', 'ProductController@add');
Route::get('/cart/edit/{id}/{action}', 'CartController@edit');
Route::get('/cart/delete/{id}', 'CartController@delete');

Route::post('/order', 'OrderController@add');