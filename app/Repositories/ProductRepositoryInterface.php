<?php

namespace App\Repositories;

/**
 * The UserRepositoryInterface contains ONLY method signatures for methods 
 * related to the User object.
 *
 * Note that we extend from RepositoryInterface, so any class that implements 
 * this interface must also provide all the standard eloquent methods (find, all, etc.)
 */
interface ProductRepositoryInterface extends RepositoryInterface {
	
	public function findProductByName($name);

}