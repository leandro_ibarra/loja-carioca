<?php

namespace App\Repositories;

use App\Abstracts\Repository as AbstractRepository;

class ProductRepository extends AbstractRepository implements ProductRepositoryInterface {

	// This is where the "magic" comes from:
	protected $modelClassName = 'Product';
	
	// This class only implements methods specific to the UserRepository
	public function findByName($name)
	{
		$where = call_user_func_array("{$this->modelClassName}::where", array($name));
		return $where->get();
	}

}