<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
* 
*/
class AppController extends Controller
{

	public function _checkCart(Request $request)
	{
		if ( !$request->session()->has('Cart.id') )
		{
			$cart = new \App\Cart;
			$cart->save();
			$request->session()->put('Cart.id', $cart->id);

			return false;
		}

		return true;
	}

	public function _getItemsAmount(Request $request)
	{
		$amount = 0;

		if ( $request->session()->has('Cart.CartItem') )
		{
			foreach ( $request->session()->get('Cart.CartItem') as $key => $value )
			{
				$amount += $value['amount'];
			}
		}

		return $amount;
	}

}