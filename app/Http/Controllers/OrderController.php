<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
* 
*/
class OrderController extends AppController
{

    function index(Request $request)
	{
		$this->_checkCart($request);
		$amount = $this->_getItemsAmount($request);

		return view('checkout', ['amount' => $amount]);
	}

	function add(Request $request)
	{
		$input = $request->input();
		
		$user = new \App\User;
		if (!$user->where('email', $input['User']['email'])->first())
		{
			$user->name = $input['User']['name'];
			$user->email = $input['User']['email'];
			$user->save($input['User']);
		}

		$order = new \App\Order;
		$user = $user->where('email', $input['User']['email'])->first();
		$order->user_id = $user->id;
		$order->save();

		$cart_item = new \App\CartItem;
		foreach ($request->session()->get('Cart.CartItem') as $key => $product)
		{
			$cart_item->cart_id = $request->session()->get('Cart.id');
			$cart_item->product_id = $product['product_id'];
			$cart_item->amount = $product['amount'];
			$cart_item->save();
		}

		$order_item = new \App\OrderItem;
		$order_item->order_id = $order->id;
		$order_item->cart_id = $request->session()->get('Cart.id');
		$order_item->save();
		
		$this->_checkCart($request);
		$amount = $this->_getItemsAmount($request);
		$request->session()->flash('success', 'Success! Your order has been successfully delivered');

		return view('checkout', ['amount' => $amount]);
	}

}
