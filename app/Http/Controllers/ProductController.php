<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
* 
*/
class ProductController extends AppController
{

	function index(Request $request)
	{
		$products = new \App\Product;

		$this->_checkCart($request);
		$amount = $this->_getItemsAmount($request);

		return view('catalog', ['products' => $products->all(), 'amount' => $amount, 'request' => $request]);
	}

	function home(Request $request)
	{
		$this->_checkCart($request);
		$amount = $this->_getItemsAmount($request);

		return view('home', ['amount' => $amount, 'request' => $request]);
	}

	function add($product_id, Request $request)
	{
		$amount =
			$request->session()->has('Cart.CartItem.'.$product_id.'.amount') ?
			$request->session()->get('Cart.CartItem.'.$product_id.'.amount') :
			0;

		if ($amount >= 5)
		{
			return 0;
		}
		
		$request->session()->put('Cart.CartItem.'.$product_id.'.amount', $amount + 1);
		$request->session()->put('Cart.CartItem.'.$product_id.'.product_id', $product_id);

		return 1;
	}

}