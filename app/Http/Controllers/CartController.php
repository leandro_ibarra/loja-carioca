<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
* 
*/
class CartController extends AppController
{
    
    function index(Request $request)
	{
		$this->_checkCart($request);
		$amount = $this->_getItemsAmount($request);

		if ( count($request->session()->get('Cart.CartItem')) == 0 )
		{
			return redirect()->action('ProductController@index')->with('warning', 'Select a product first!');
		}

		$_products = new \App\Product;
		$products = array();

		foreach ($request->session()->get('Cart.CartItem') as $key => $value)
		{
			$products[$key] = $_products->find($key)->toArray();
			$products[$key]['amount'] = $value['amount'];
		}

		return view('cart', ['amount' => $amount, 'products' => $products]);
	}

	function edit($product_id, $action, Request $request)
	{
		$amount = $request->session()->get('Cart.CartItem.'.$product_id.'.amount');

		if ($action == 'plus')
		{
			$request->session()->put('Cart.CartItem.'.$product_id.'.amount', $amount + 1);
		}
		elseif ($action == 'minus')
		{
			$request->session()->put('Cart.CartItem.'.$product_id.'.amount', $amount - 1);
		}

		if ($request->session()->get('Cart.CartItem.'.$product_id.'.amount') == 0)
		{
			$this->delete($product_id, $request);
		}
	}

	function delete($product_id, Request $request)
	{
		$request->session()->forget('Cart.CartItem.'.$product_id);
		if ( count($request->session()->get('Cart.CartItem'))==0 )
		{
			$request->session()->flash('warning', 'Add products from our catalog to continue');
		}
	}

}
