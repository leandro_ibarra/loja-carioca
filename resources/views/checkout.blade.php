@extends('layouts.app')

@section('content')
<div class="page-header">
	<h1 class="text-center">Checkout</h1>
</div>

<form id="formCheckout" method="post" action="/order">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="form-group">
				<label for="inputName">Name</label>
				<input type="text" class="form-control" name="User[name]" id="inputName" placeholder="Enter full name" required>
			</div>
		</div>
		<div class="col-md-6 col-md-offset-3">
			<div class="form-group">
				<label for="inputEmail">Email address</label>
				<input type="email" class="form-control" name="User[email]" id="inputEmail" placeholder="Enter email" required>
			</div>
		</div>
		<input name="_token" type="hidden" value="{{ csrf_token('authenticate') }}">
	</div>
	<div class="row">
		<div class="col-md-4 col-md-offset-3">
			<div><button type="submit" class="btn btn-primary">Submit</button></div>
		</div>
	</div>
</form>

<div class="row">
	<div class="col-md-2 col-md-offset-5">
		<div class="text-center"><a href="/cart">Back</a></div>
	</div>
</div>

<script>
/*
var ContactForm = function () {

	return {

		//Contact Form
		initContactForm: function () {

			// Validation
			$("#formCheckout").validate({

				// Rules for form validation
				rules:
				{
					"subscriptor[nombre]":
					{
						required: true
					},
					"subscriptor[email]":
					{
						required: true,
						email: true
					},
					"mensaje":
					{
						required: true,
						minlength: 10
					}
				},

				// Messages for form validation
				messages:
				{
					"subscriptor[nombre]":
					{
						required: 'Por favor, ingrese su nombre',
					},
					"subscriptor[email]":
					{
						required: 'Por favor, ingrese su correo electrónico',
						email: 'Por favor, ingrese un correo electrónico VÁLIDO'
					},
					"mensaje":
					{
						required: 'Por favor, ingrese su mensaje',
						minlength: 'Por favor, ingrese al menos 10 caracteres'
					}
				},

				// Ajax form submition
				submitHandler: function(form)
				{
					var nombre = $('#subscriptor-nombre').val();
					var email = $('#subscriptor-email').val();
					var mensaje = $('#mensaje').val();

					$(form).ajaxSubmit(
					{
						url: '/consultas/add',
						data: { "subscriptor[nombre]": nombre, "subscriptor[email]": email, "mensaje": mensaje },
						type: 'POST',

						beforeSend: function()
						{
							$('#formCheckout button[type="submit"]').attr('disabled', true);
						},
						success: function()
						{
							window.location.replace('/');
						}
					});
				},

				errorPlacement: function(error, element)
				{
					error.insertAfter(element.parent());
				}
			});
			return false;
		}

	};

}();
*/
</script>
<script>
/*
	jQuery(document).ready(function() {
		ContactForm.initContactForm();
	});
*/
</script>
@endsection