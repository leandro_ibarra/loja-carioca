<!DOCTYPE html>
<html>
<head>
	<title>Loja Carioca</title>
	{{ Html::style('css/bootstrap.min.css') }}
	{{ Html::script('js/jquery.min.js') }}
	<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>

	<div class="container">

		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="/">Loja Carioca</a>
				</div>
				<ul class="nav navbar-nav">
					<li><a href="/catalog">Catalog</a></li>
					<li><a href="/cart">Cart</a></li>
					<li><a href="/checkout">Checkout</a></li>
					<li><a href="#"><span class="glyphicon glyphicon-shopping-cart"> <span id="cart-amount">{{ $amount }}</span></span></a></li>
				</ul>
			</div>
		</nav>

		@if (session('success'))
		<div class="alert alert-success">
			{{ session('success') }}
		</div>
		@endif

		@if (session('warning'))
		<div class="alert alert-warning">
			{{ session('warning') }}
		</div>
		@endif

		@yield('content')

	</div>
	
</body>
</html>