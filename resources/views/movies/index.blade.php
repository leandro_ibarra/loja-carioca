<!DOCTYPE html>
<html>
<head>
	<title>My Movies</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>

	<div class="container">
		<div class="page-header">
			<h1>My Movies</h1>
		</div>

		<ul>
		@foreach ($movies as $movie)
			<li class="list-group-item">
				<h2 class="list-group-item-heading">{{ $movie->title }}</h2>
				<p class="list-group-item-text">Released on: {{ date('F d, Y', strtotime($movie->released_on)) }}</p>
				<p>Rating: {{ $movie->rating }}</p>
				<p>Review: {{ $movie->review }}</p>
			</li>
		@endforeach
		</ul>
	</div>
</body>
</html>