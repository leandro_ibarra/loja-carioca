@extends('layouts.app')

@section('content')
<div class="page-header">
	<h1 class="text-center">Catalog</h1>
</div>

<ul class="row">
	@foreach ($products as $product)
	<li class="col-lg-3 col-md-4 col-sm-4 col-xs-6 list-group-item">
		<img src="/img/product.png" style="max-width: 100%;">
		<h3 class="list-group-item-heading">{{ $product->name }}</h3>
		<p class="list-group-item-text">R$ {{ number_format($product->price, 2, ',', '.') }}</p>
		<div class="row">
			<div class="btn-group col-md-6 col-md-offset-4 col-sm-6 col-sm-offset-4" role="group" aria-label="">
				<button type="button" class="btn btn-default text-center add" data-id="{{ $product->id }}" >Add</button>
			</div>
		</div>
	</li>
	@endforeach
</ul>

<div class="row">
	<div class="col-md-2 col-md-offset-5">
		<div class="text-center"><a href="/cart">Next</a></div>
	</div>
</div>

<script>
	$('.add').click(function (event) {

		event.preventDefault();
		product_id = $(this).attr('data-id');

		$.ajax({

			url: '/product/add/' + product_id,

			success: function(ev) {
				if(ev=='0')
				{
					alert("You can't add more than 5 units per item");
				}
				else
				{
					$("#cart-amount").text(parseInt($("#cart-amount").text())+1);
				}
			}
		});

		return false;

	});
</script>
@endsection