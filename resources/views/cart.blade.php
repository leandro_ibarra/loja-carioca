@extends('layouts.app')

@section('content')
<div class="page-header">
	<h1 class="text-center">Cart</h1>
</div>

<table class="table">
	<thead>
		<tr>
			<th>#</th>
			<th>Product</th>
			<th>Price</th>
			<th class="text-center">Action</th>
		</tr>
	</thead>
	<tbody id="cart-detail">
		@foreach ($products as $product)
		<tr id="{{ $product['id'] }}">
			<th scope="row">
				<div id="item-amount" class="col-md-1">{{ $product['amount'] }}</div>
				<div class="col-md-1">
					<button type="button" class="btn btn-xs glyphicon glyphicon-plus plus" data-id="{{ $product['id'] }}" {{ $product['amount'] >= 5 ? 'disabled="disabled"' : '' }}></button>
				</div>
				<div class="col-md-1">
					<button type="button" class="btn btn-xs glyphicon glyphicon-minus minus" data-id="{{ $product['id'] }}"></button>
				</div>
			</th>
			<td>{{ $product['name'] }}</td>
			<td>R$ {{ $product['price'] }}</td>
			<td class="text-center">
				<button type="button" title="Remove from cart" class="btn btn-xs glyphicon glyphicon-trash delete" data-id="{{ $product['id'] }}"></button>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

<div class="row">
	<div class="col-md-1 col-md-offset-5">
		<div class="text-center"><a href="/catalog">Back</a></div>
	</div>
	<div class="col-md-1">
		<div class="text-center"><a href="/checkout">Next</a></div>
	</div>
</div>

<script>
	$('.plus').click(function (event) {

		event.preventDefault();
		product_id = $(this).attr('data-id');

		$.ajax({

			url: '/cart/edit/' + product_id + '/plus',

			success: function(ev) {
				$("#item-amount").text(parseInt($("#item-amount").text())+1);
				$("#cart-amount").text(parseInt($("#cart-amount").text())+1);
				if ($("#item-amount").text()>='5')
				{
					$(".plus").attr('disabled', true);
				}
			}
		});

		return false;

	});
	$('.minus').click(function (event) {

		event.preventDefault();
		product_id = $(this).attr('data-id');

		$.ajax({

			url: '/cart/edit/' + product_id + '/minus',

			success: function(ev) {
				$("#item-amount").text(parseInt($("#item-amount").text())-1);
				$("#cart-amount").text(parseInt($("#cart-amount").text())-1);
				if ($("#item-amount").text()=='0')
				{
					$('#' + product_id).remove();
					if (!$.trim($('#cart-detail').html()))
					{
						window.location.replace('/catalog');
					}
				}
				else if ($("#item-amount").text()<'5')
				{
					$(".plus").removeAttr('disabled');
				}
			}
		});

		return false;

	});
	$('.delete').click(function (event) {

		event.preventDefault();
		product_id = $(this).attr('data-id');

		$.ajax({

			url: '/cart/delete/' + product_id,

			success: function(ev) {
				$("#cart-amount").text(parseInt($("#cart-amount").text())-parseInt($("#item-amount").text()));
				$('#' + product_id).remove();
				if (!$.trim($('#cart-detail').html()))
				{
					window.location.replace('/catalog');
				}
			}
		});

		return false;

	});
</script>
@endsection