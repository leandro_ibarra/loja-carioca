# Loja Carioca Exercise


Loja Carioca is a small e-commerce company that sells tanning lotions to their customers in Rio de Janeiro. 
As business grows, the company is looking forward to building a shopping cart for their old fashioned website. This new feature will allow customers to purchase multiple products in a single order and trigger emails to both customer and store administrators.

In order to accomplish this goal we are looking for a well-trained developer to help us build this new feature.

The application must have the following pages:

#### Catalog

Containing the following product: Hawaiian Tropic Dark Tanning Oil, Original 8 fl oz (237 ml), R$ 40,00. Customers should be able to add this product to the cart (up to 5 items if necessary)

#### Cart

Customers should be able to remove an item from the cart as well as increase/decrease desired quantity.

#### Checkout

Form with Customer's name and email. Once submitted, the app should trigger a confirmation email to the customer and to the store administrator. 

In order to track future orders, the customer and order info needs to be stored in the application for later usage.



## Technical Requirements

The solution must be built in Laravel 5.2 and Bootstrap.

#### HTML/CSS

We don't need you to write CSS code. Bootstrap markup should do the job.

#### Application

The following Models must be present:
- Product
- Cart
- Cart Item
- User
- Order
- Order Item

In order to read or write data from/to the DB, models should be encapsulated into repositories (Repository Pattern). See also: http://slashnode.com/reusable-repository-design-in-laravel/

Also, we expect you to use View Presenters for outputting data inside views (Decorator Pattern). See also: https://github.com/laravel-auto-presenter/laravel-auto-presenter

#### Tests

Finally, we need you to write the following tests to make sure everything works as expected.

1. testCartIsEmpty()
2. testAddItemToCart()
3. testRemoveItemFromCart()
